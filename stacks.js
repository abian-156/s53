let collection = [];


//add element in the stacks.
function addToStack(element){
	collection.push(element)
	return collection;
}

//this will remove element from the stack
function removeFromStack(element) {
	collection.pop();
	return collection;
} 

//the element located at the very top of a stack is called 'peek'


function peek() {
	return collection[collection.length - 1];
}


// to get the number of element in a stack

function getStackLength() {
	return collection.length;
}

addToStack('One');
addToStack('Two');
addToStack('Three');
addToStack('Four');
console.log(getStackLength());
//removeFromStack();
console.log(peek());
console.log(collection);